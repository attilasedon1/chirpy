package database

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"
	"sort"

	"golang.org/x/crypto/bcrypt"
)

type BaseUser struct {
	Email       string `json:"email"`
	IsChirpyRed bool   `json:"is_chirpy_red"`
}

type Chirp struct {
	Id       int    `json:"id"`
	Body     string `json:"body"`
	AuthorId int    `json:"author_id"`
}

type User struct {
	Id int `json:"id"`
	BaseUser
	Password string `json:"password"`
}

type UserResponse struct {
	Id int `json:"id"`
	BaseUser
}

type UserResponseWithToken struct {
	UserResponse
	Token        string `json:"token"`
	RefreshToken string `json:"refresh_token"`
}

type DB struct {
	path string
	mux  *sync.RWMutex
}

type DBStructure struct {
	Chirps        map[int]Chirp        `json:"chirps"`
	Users         map[int]User         `json:"users"`
	RefreshTokens map[string]time.Time `json:"refresh_tokens"`
}

// NewDB creates a new database connection
// and creates the database file if it doesn't exist
func NewDB(path string) (*DB, error) {
	db := DB{}
	db.path = path
	err := db.ensureDB()
	if err != nil {
		return nil, err
	} else {
		db.mux = &sync.RWMutex{}
		return &db, nil
	}
}

// CreateChirp creates a new chirp and saves it to disk
func (db *DB) CreateChirp(body string, authorId int) (Chirp, error) {
	chirp := Chirp{}
	chirp.Body = body
	chirp.AuthorId = authorId
	chirpDBStruct, err := db.loadDB()
	if err != nil {
		return chirp, err
	} else {
		numOfKeys := len(chirpDBStruct.Chirps)
		chirp.Id = numOfKeys + 1
		chirpDBStruct.Chirps[numOfKeys+1] = chirp
		writeErr := db.writeDB(chirpDBStruct)
		if writeErr != nil {
			return chirp, writeErr
		} else {
			return chirp, nil
		}
	}
}

// ReadChirps returns all chirps in the database
func (db *DB) ReadChirps() ([]Chirp, error) {
	chirpDB, err := db.loadDB()
	if err != nil {
		return nil, err
	}
	chirps := make([]Chirp, 0)
	for _, val := range chirpDB.Chirps {
		chirps = append(chirps, val)
	}
	sort.Slice(chirps, func(i,j int) bool {
		return chirps[i].Id < chirps[j].Id 
	})
	return chirps, nil
}

func (db *DB) ReadChirpsByAuthorId(id int) ([]Chirp, error) {
	chirpDB, err := db.loadDB()
	if err != nil {
		return nil, err
	}
	chirps := make([]Chirp, 0)
	for _, val := range chirpDB.Chirps {
		if val.AuthorId == id {
			chirps = append(chirps, val)
		}
	}
	sort.Slice(chirps, func(i,j int) bool {
		return chirps[i].Id < chirps[j].Id 
	})
	return chirps, nil
}

func (db *DB) ReadChirpById(id int) (Chirp, error) {
	chirpDB, err := db.loadDB()
	if err != nil {
		return Chirp{}, err
	}
	chirp, ok := chirpDB.Chirps[id]
	if !ok {
		return chirp, fmt.Errorf("Element with %d id cannot be found", id)
	}
	return chirp, nil
}

func (db *DB) DeleteChirpById(id int) error {
	chirpDB, err := db.loadDB()
	if err != nil {
		return err
	} else {
		delete(chirpDB.Chirps, id)
		return nil
	}
}

func (db *DB) CreateUser(email string, password string) (UserResponse, error) {
	user := User{}
	userResp := UserResponse{}
	userResp.Email = email
	user.Email = email
	pass, perr := bcrypt.GenerateFromPassword([]byte(password), 8)
	if perr != nil {
		return userResp, perr
	}
	user.Password = string(pass)
	chirpDBStruct, err := db.loadDB()
	if err != nil {
		return userResp, err
	} else {
		numOfKeys := len(chirpDBStruct.Users)
		user.Id = numOfKeys + 1
		userResp.Id = user.Id
		chirpDBStruct.Users[numOfKeys+1] = user
		writeErr := db.writeDB(chirpDBStruct)
		if writeErr != nil {
			return userResp, writeErr
		} else {
			return userResp, nil
		}
	}
}

func (db *DB) GetUser(email string, password string) (UserResponseWithToken, error) {
	chirpDBStruct, err := db.loadDB()
	if err != nil {
		return UserResponseWithToken{}, err
	}
	for _, user := range chirpDBStruct.Users {
		if user.Email == email && bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)) == nil {
			userResp := UserResponseWithToken{}
			userResp.Email = email
			userResp.Id = user.Id
			userResp.IsChirpyRed = user.IsChirpyRed
			return userResp, nil
		}
	}
	return UserResponseWithToken{}, fmt.Errorf("Not found")
}

func (db *DB) UpgradeUser(id int, isChirpyRed bool) (UserResponse, error) {
	chirpDBStruct, err := db.loadDB()
	if err != nil {
		return UserResponse{}, err
	}
	if entry, ok := chirpDBStruct.Users[id]; ok {
		entry.IsChirpyRed = isChirpyRed
		chirpDBStruct.Users[id] = entry
		writeErr := db.writeDB(chirpDBStruct)
		userResp := UserResponse{}
		userResp.Email = entry.Email
		userResp.Id = entry.Id
		userResp.IsChirpyRed = entry.IsChirpyRed
		if writeErr != nil {
			return userResp, writeErr
		}
	} else {
		return UserResponse{}, fmt.Errorf("Not found")
	}
	return UserResponse{}, nil
}

func (db *DB) UpdateUser(id int, email string, password string) (UserResponse, error) {

	userResp := UserResponse{}
	userResp.Email = email
	userResp.Id = id
	chirpDBStruct, err := db.loadDB()
	if err != nil {
		return UserResponse{}, err
	}
	pass, perr := bcrypt.GenerateFromPassword([]byte(password), 8)
	if perr != nil {
		return UserResponse{}, perr
	}
	if entry, ok := chirpDBStruct.Users[id]; ok {
		entry.Email = email
		if password != "" {
			entry.Password = string(pass)
		}
		chirpDBStruct.Users[id] = entry
		writeErr := db.writeDB(chirpDBStruct)
		if writeErr != nil {
			return userResp, writeErr
		}
	} else {
		return userResp, fmt.Errorf("Not found")
	}
	return userResp, nil
}

func (db *DB) AddRefreshToken(token string) error {
	chirpStructure, err := db.loadDB()
	if err != nil {
		return err
	}
	chirpStructure.RefreshTokens[token] = time.Time{}
	writeErr := db.writeDB(chirpStructure)
	if writeErr != nil {
		return writeErr
	}
	return nil
}

func (db *DB) GetRefreshToken(token string) (time.Time, error) {
	chirpStructure, err := db.loadDB()
	if err != nil {
		return time.Time{}, err
	}
	if time, ok := chirpStructure.RefreshTokens[token]; ok {
		return time, nil
	} else {
		return time, fmt.Errorf("Token not found")
	}
}

func (db *DB) RevokeRefreshToken(token string) error {
	chirpStructure, err := db.loadDB()
	if err != nil {
		return err
	}
	if _, ok := chirpStructure.RefreshTokens[token]; ok {
		chirpStructure.RefreshTokens[token] = time.Now()
		writeErr := db.writeDB(chirpStructure)
		if writeErr != nil {
			return writeErr
		}
	} else {
		return fmt.Errorf("Token not found")
	}
	return nil
}

// ensureDB creates a new database file if it doesn't exist
func (db *DB) ensureDB() error {
	if fileExists(db.path) {
		return nil
	} else {
		file, err := os.Create(db.path)
		if err != nil {
			return err
		} else {
			dbSt := DBStructure{}
			dbSt.Chirps = make(map[int]Chirp)
			dbSt.Users = make(map[int]User)
			dbSt.RefreshTokens = make(map[string]time.Time)
			b, err := json.Marshal(&dbSt)
			if err != nil {
				return err
			}
			file.Write(b)
			return nil
		}
	}
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// loadDB reads the database file into memory
func (db *DB) loadDB() (DBStructure, error) {
	bytes, err := os.ReadFile(db.path)
	chirpDB := DBStructure{}
	if err != nil {
		return chirpDB, err
	} else {
		marshalErr := json.Unmarshal(bytes, &chirpDB)
		if marshalErr != nil {
			return chirpDB, marshalErr
		} else {
			return chirpDB, nil
		}
	}
}

// writeDB writes the database file to disk
func (db *DB) writeDB(dbStructure DBStructure) error {
	data, marshErr := json.Marshal(dbStructure)
	if marshErr != nil {
		return marshErr
	} else {
		db.mux.RLock()
		os.WriteFile(db.path, data, 0777)
		db.mux.RUnlock()
		return nil
	}
}

func DestroyDB(filename string) error {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return nil
	}
	deleteErr := os.Remove(filename)
	if deleteErr != nil {
		return deleteErr
	}
	return nil
}
