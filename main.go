package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"strings"

	"chirpy/database"
	"chirpy/utils"
	"strconv"

	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/golang-jwt/jwt/v5"
	"github.com/joho/godotenv"
)

type apiConfig struct {
	fileserverHits int
}

type ReqChirp struct {
	Body string `json:"body"`
}

type UserRequest struct {
	database.BaseUser
	ExpiresInSeconds int    `json:"expires_in_seconds"`
	Password         string `json:"password"`
}

type TemporaryUserResponse struct {
	Email string `json:"email"`
  Id int `json:"id"`
}

type AccessTokenResponse struct {
	Token string `json:"token"`
}

type PolkaRequestData struct {
	UserId int `json:"user_id"`
}

type PolkaRequestResponse struct {
	Event string           `json:"event"`
	Data  PolkaRequestData `json:"data"`
}

func (cfg *apiConfig) middlewareMetricsInc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cfg.fileserverHits++
		next.ServeHTTP(w, r)
	})
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(200)
	w.Write([]byte("OK"))
}

func metricsFactory(cfg *apiConfig) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "text/html; charset=utf-8")
		w.WriteHeader(200)
		w.Write([]byte(fmt.Sprintf("<html><body><h1>Welcome, Chirpy Admin</h1><p>Chirpy has been visited %d times!</p></body></html>", cfg.fileserverHits)))
	}
}

func resetFactory(cfg *apiConfig) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "text/plain; charset=utf-8")
		w.WriteHeader(200)
		cfg.fileserverHits = 0
		w.Write([]byte("Metrics reset done"))
	}
}

func createChirps(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		id, perr := validateAccessToken(r)
		if perr != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		decoder := json.NewDecoder(r.Body)
		chirp := database.Chirp{}
		err := decoder.Decode(&chirp)
		if err != nil {
			utils.RespondWithError(w, 500, "Something went wrong")
			return
		}

		if len(chirp.Body) > 140 {
			utils.RespondWithError(w, 400, "Chirp is too long")
		} else {
			respBody, createErr := db.CreateChirp(utils.HandleBadwords(chirp.Body), id)
			if createErr != nil {
				utils.RespondWithError(w, 500, "Database Error")
			} else {
				utils.RespondWithJSON(w, 201, respBody)
			}
		}
	}
}

func getChirps(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		s := r.URL.Query().Get("author_id")
		isDesc := r.URL.Query().Get("sort") == "desc"
		if s != "" {
			val, cerr := strconv.Atoi(s)
			if cerr != nil {
				utils.RespondWithError(w, 400, "Bad Request")
				return
			}
			respBody, err := db.ReadChirpsByAuthorId(val)
			if err != nil {
				utils.RespondWithError(w, 500, "Internal Server Error")
			} else {
				if isDesc {
					utils.Reverse(respBody)
				}
				utils.RespondWithJSON(w, 200, respBody)
			}
		} else {
			respBody, err := db.ReadChirps()
			if err != nil {
				utils.RespondWithError(w, 500, "Database Error")
			} else {
				if isDesc {
					utils.Reverse(respBody)
				}
				utils.RespondWithJSON(w, 200, respBody)
			}
		}
	}
}

func getChirpById(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")
		intId, convErr := strconv.Atoi(id)
		if convErr != nil {
			utils.RespondWithError(w, 400, convErr.Error())
			return
		}
		chirp, err := db.ReadChirpById(intId)
		if err != nil {
			utils.RespondWithError(w, 404, err.Error())
		}
		utils.RespondWithJSON(w, 200, chirp)
	}
}

func deleteChirpById(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")
		intId, convErr := strconv.Atoi(id)
		if convErr != nil {
			utils.RespondWithError(w, 400, convErr.Error())
			return
		}
		authorId, err := validateAccessToken(r)
		if err != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		chirp, err := db.ReadChirpById(intId)
		if err != nil {
			utils.RespondWithError(w, 404, "Not Found")
			return
		}
		if chirp.AuthorId != authorId {
			utils.RespondWithError(w, 403, "Forbidden")
			return
		}
		derr := db.DeleteChirpById(intId)
		if derr != nil {
			utils.RespondWithError(w, 500, "Internal Server Error")
			return
		}
		utils.RespondWithJSON(w, 200, "OK")
	}
}

func createUsers(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		user := database.User{}
		err := decoder.Decode(&user)
		if err != nil {
			utils.RespondWithError(w, 500, "Something went wrong")
			return
		}
		respBody, createErr := db.CreateUser(user.Email, user.Password)
		if createErr != nil {
			utils.RespondWithError(w, 500, "Database Error")
		} else {
			// Temporary
			userResp := TemporaryUserResponse{}
			userResp.Email = respBody.Email
			userResp.Id = respBody.Id
			// utils.RespondWithJSON(w, 201, respBody)
			utils.RespondWithJSON(w, 201, userResp)
		}
	}
}

func mintToken(id string, issuer string, db *database.DB, expiresInSeconds int) (string, error) {
	if issuer != "chirpy-access" && issuer != "chirpy-refresh" {
		return "", fmt.Errorf("Invalid issuer")
	}
	godotenv.Load()
	jwtSecret := os.Getenv("JWT_SECRET")
	claims := jwt.RegisteredClaims{}
	claims.Issuer = issuer
	claims.IssuedAt = jwt.NewNumericDate(time.Now().UTC())
	if issuer == "chirpy-access" && (expiresInSeconds > 3600 || expiresInSeconds == 0) {
		expiresInSeconds = 3600
	} else if issuer == "chirpy-refresh" && (expiresInSeconds > 60*86400 || expiresInSeconds == 0) {
		expiresInSeconds = 60 * 86400
	}
	claims.ExpiresAt = jwt.NewNumericDate(claims.IssuedAt.Add(time.Second * time.Duration(expiresInSeconds)))
	claims.Subject = id
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		return "", err
	} else {
		if issuer == "chirpy-access" {
			return tokenString, nil
		} else {
			err := db.AddRefreshToken(tokenString)
			if err != nil {
				return "", err
			}
			return tokenString, nil
		}
	}
}

func login(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		user := UserRequest{}
		err := decoder.Decode(&user)
		if err != nil {
			utils.RespondWithError(w, 500, "Something went wrong")
			return
		}

		respBody, respErr := db.GetUser(user.Email, user.Password)
		if respErr != nil {
			if respErr.Error() == "Not found" {
				utils.RespondWithError(w, 401, "Unauthorized")
			} else {
				utils.RespondWithError(w, 500, "Internal Server Error")
			}
		} else {
			token, err := mintToken(fmt.Sprintf("%v", respBody.Id), "chirpy-access", db, user.ExpiresInSeconds)
			if err != nil {
				utils.RespondWithError(w, 500, "Internal Server Error")
				return
			}
			respBody.Token = token
			refreshToken, err := mintToken(fmt.Sprintf("%v", respBody.Id), "chirpy-refresh", db, user.ExpiresInSeconds)
			if err != nil {
				utils.RespondWithError(w, 500, "Internal Server Error")
				return
			}
			respBody.RefreshToken = refreshToken
			utils.RespondWithJSON(w, 200, respBody)
		}
	}
}

func parseAuthorizationHeader(r *http.Request) (*jwt.Token, error) {
	bearerToken := r.Header.Get("Authorization")
	jwtToken := strings.Split(bearerToken, " ")[1]
	claims := jwt.RegisteredClaims{}
	return jwt.ParseWithClaims(jwtToken, &claims, func(token *jwt.Token) (interface{}, error) {
		jwtSecret := os.Getenv("JWT_SECRET")
		return []byte(jwtSecret), nil
	})
}

func validateAccessToken(r *http.Request) (val int, err error) {
	token, err := parseAuthorizationHeader(r)
	if err != nil {
		return 0, err
	}
	if !token.Valid {
		return 0, fmt.Errorf("Token is invalid")
	}
	if issuer, err := token.Claims.GetIssuer(); err != nil || issuer != "chirpy-access" {
		return 0, fmt.Errorf("Invalid token type")
	}
	id, serr := token.Claims.GetSubject()
	if serr != nil {
		return 0, serr
	}
	val, cerr := strconv.Atoi(id)
	if cerr != nil {
		return 0, cerr
	}
	return val, nil
}

func refreshAccessToken(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		token, err := parseAuthorizationHeader(r)
		if err != nil || !token.Valid {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		l_time, err := db.GetRefreshToken(token.Raw)
		if err != nil || !l_time.IsZero() {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		id, err := token.Claims.GetSubject()
		if err != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
		}
		newToken, err := mintToken(id, "chirpy-access", db, 0)
		if err != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		respBody := AccessTokenResponse{}
		respBody.Token = newToken
		utils.RespondWithJSON(w, 200, respBody)
	}
}

func revokeAccessToken(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		token, err := parseAuthorizationHeader(r)
		if err != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		rerr := db.RevokeRefreshToken(token.Raw)
		if rerr != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		utils.RespondWithJSON(w, 200, "OK")
		return
	}
}

func updateUser(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		user := database.User{}
		err := decoder.Decode(&user)
		if err != nil {
			utils.RespondWithError(w, 500, "Something went wrong")
			return
		}

		val, err := validateAccessToken(r)
		if err != nil {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}

		respBody, respErr := db.UpdateUser(val, user.Email, user.Password)
		if respErr != nil {
			utils.RespondWithError(w, 500, "Internal Server Error")
		} else {
			utils.RespondWithJSON(w, 200, respBody)
		}
	}
}

func upgradeUser(db *database.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		polka := PolkaRequestResponse{}
		err := decoder.Decode(&polka)
		if err != nil {
			utils.RespondWithError(w, 500, "Something went wrong")
			return
		}

		bearerToken := r.Header.Get("Authorization")
		if strings.Trim(bearerToken, " ") == "" {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}
		apikey := strings.Split(bearerToken, " ")[1]
		godotenv.Load()
		polkaKey := os.Getenv("POLKA_KEY")
		if apikey != polkaKey {
			utils.RespondWithError(w, 401, "Unauthorized")
			return
		}

		if polka.Event == "user.upgraded" {
			respBody, respErr := db.UpgradeUser(polka.Data.UserId, true)
			if respErr != nil && respErr.Error() == "Not found" {
				utils.RespondWithError(w, 404, "Not Found")
			} else if respErr != nil {
				utils.RespondWithError(w, 500, "Internal Server Error")
			} else {
				utils.RespondWithJSON(w, 200, respBody)
			}
		} else {
			utils.RespondWithJSON(w, 200, "OK")
		}
	}
}

func handleDebugMode(path string) {
	dbg := flag.Bool("debug", false, "Enable debug mode")
	flag.Parse()
	isDebug := *dbg
	if isDebug {
		database.DestroyDB(path)
	}
}

func main() {

	dbPath := "./database/database.json"
	handleDebugMode(dbPath)

	apiCfg := apiConfig{}

	// create DB
	db, dberr := database.NewDB(dbPath)
	if dberr != nil {
		fmt.Println(dberr.Error())
		return
	}

	// Chi routing
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World!"))
	})
	// A "Handle" függvénnyel adunk neki end pointot
	// FileServer-rel megmondjuk, hogy fájlt adjon vissza,
	// és mivel a directory-t adtuk vissza így defaultol az index.html-re
	// A file server furán működik, ez a strippelés ez fura nekem.
	fs := http.FileServer(http.Dir("app/"))
	r.Handle("/app/*", apiCfg.middlewareMetricsInc(http.StripPrefix("/app", fs)))
	r.Handle("/app", apiCfg.middlewareMetricsInc(http.StripPrefix("/app", fs)))

	apiRouter := chi.NewRouter()

	// HandleFunc-kal is endpointot adunk meg de itt megadhatunk komplex logikákat
	apiRouter.Get("/healthz", healthHandler)
	apiRouter.HandleFunc("/reset", resetFactory(&apiCfg))
	apiRouter.Post("/chirps", createChirps(db))
	apiRouter.Get("/chirps", getChirps(db))
	apiRouter.Get("/chirps/{id}", getChirpById(db))
	apiRouter.Delete("/chirps/{id}", deleteChirpById(db))
	apiRouter.Post("/users", createUsers(db))
	apiRouter.Put("/users", updateUser(db))
	apiRouter.Post("/login", login(db))
	apiRouter.Post("/refresh", refreshAccessToken(db))
	apiRouter.Post("/revoke", revokeAccessToken(db))

	apiRouter.Post("/polka/webhooks", upgradeUser(db))

	r.Mount("/api", apiRouter)

	adminRouter := chi.NewRouter()

	adminRouter.Get("/metrics", metricsFactory(&apiCfg))

	r.Mount("/admin", adminRouter)

	// mindent megengedünk, hogy ne kapjunk CORS-ot
	corsMux := middlewareCors(r)

	// Szerver indítása
	err := http.ListenAndServe(":8080", corsMux)
	fmt.Println(err)
}
