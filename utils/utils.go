package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type ErrorResponse struct {
	Error string `json:"error"`
}

func RespondWithError(w http.ResponseWriter, code int, msg string) {
	respBody := ErrorResponse{
		Error: msg,
	}
	w.WriteHeader(code)
	dat, err := json.Marshal(respBody)
	if err != nil {
		fmt.Printf("Error marshalling JSON: %s", err)
		w.WriteHeader(500)
	} else {
		w.Write(dat)
	}
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	dat, err := json.Marshal(payload)
	if err != nil {
		fmt.Printf("Error marshalling JSON: %s", err)
		w.WriteHeader(500)
	} else {
		w.WriteHeader(code)
		w.Write(dat)
	}
}

func Contains(src []string, ch string) bool {
	for _, s := range src {
		if s == ch {
			return true
		}
	}
	return false
}

func Reverse[S ~[]E, E any](s S)  {
    for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
        s[i], s[j] = s[j], s[i]
    }
}

func HandleBadwords(chirp string) string {

	badWordSlice := []string{
		"kerfuffle",
		"sharbert",
		"fornax",
	}

	chirpSlice := strings.Split(chirp, " ")
	for i, word := range chirpSlice {
		if Contains(badWordSlice, strings.ToLower(word)) {
			chirpSlice[i] = "****"
		}
	}
	return strings.Join(chirpSlice, " ")

}
